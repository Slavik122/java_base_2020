package Classes;

import Classes.Point;

public abstract class Shape {

	public Point position;

	public Point getPosition() {
		return position;
	}

	public abstract double calculateArea();

}
