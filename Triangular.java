package Classes;

public class Triangular extends Shape {
	
	public double width;
	public double height;
	
	@Override
	public double calculateArea() {
		return 1/2 * width * height;
	}
	
}
