package Classes;

public class Point {

	private long x;
	private long y;

	public long getX() {
		return this.x;
	}

	public long getY() {
		return this.y;
	}
}
